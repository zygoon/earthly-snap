<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: Zygmunt Krynicki
-->

# What is this?

This is Earthly packaged as a snap application.

## Building

You will need a system capable of running Snapcraft and LXD.  On either x86\_64
or aarch64 systems you may run `snapcraft` to build the package.

You may also build the package for aarch64 (aka arm64) with:

```
snapcraft --verbose --build-for=arm64
```

You may also build the package for x86\_64 (aka amd64) with:

```
snapcraft --verbose --build-for=amd64
```

## Installation

To install the locally built snap run:

```
sudo snap install --dangerous ./earthly_*.snap
```

You will then need to follow-up with one-time setup. For details run: 

```
snap info earthly
```
